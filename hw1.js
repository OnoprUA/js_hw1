
// EXERCISE 1
// Оголосіть дві змінні: admin та name. 
// Встановіть ваше ім'я в якості значення змінної name. 
// Скопіюйте це значення в змінну admin і виведіть його в консоль.

const name = "Oleksandr";
const admin = name;
console.log(admin);

// EXERCISE 2
// Оголосити змінну days і ініціалізувати її числом від 1 до 10. 
// Перетворіть це число на кількість секунд і виведіть на консоль.

// Option 1
let day = 5;
console.log(day*24*60**2);

// Option 2
let days = +prompt("enter number since 1 till 10");
if (days >=1 && days<=10){
    let seconds = days * 24 * 60**2;
    console.log(seconds);
    alert("Congrats! "+ seconds + " in " + days + " day(s)");
} else {
    alert("Incorrect input, please try againe");
}

// EXERCISE3
// Запитайте у користувача якесь значення і виведіть його в консоль.

let userNumber = prompt("Please enter any value");
console.log(userNumber);

// Answers on theoretical guestion in the file README.md
